import BuildControl from './BuildControl/BuildControl';
import './BuildControls.css';

const controls = [
    {label: 'Salad', type: 'salad'},
    {label: 'Bacon', type: 'bacon'},
    {label: 'Cheese', type: 'cheese'},
    {label: 'Meat', type: 'meat'},
]
const buildControls = props => {
    return (
        <div className="BuildControls">
            <h2>Current Price: {props.price.toLocaleString('en-US', { style: 'currency', currency: 'USD' })}</h2>    
            {controls.map(control => {
                return <BuildControl 
                    key={control.label} 
                    label={control.label} 
                    added={() => props.ingredientAdded(control.type)} 
                    removed={() => props.ingredientRemoved(control.type)}
                    disabled={props.disabled[control.type]}/>
            })}    
            <button className="OrderButton" disabled={!props.purchasable} onClick={props.ordered}>Order Now</button>
        </div>
        
    );  
}

export default buildControls;
