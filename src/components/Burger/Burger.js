import BurgerIngredient from './BurgerIngredient/BurgerIngredient';
import './Burger.css';

const Burger = props => {
    let transformedIngredients = Object.keys(props.ingredients)
        .map(igKey => {
            return [...Array(props.ingredients[igKey])].map((_, i) => {
                return <BurgerIngredient key={igKey + i} type={igKey} ></BurgerIngredient>;
            });
        })
        .reduce((arr, el) => {
            return arr.concat(el);
        });
    if(transformedIngredients.length === 0) {
        transformedIngredients = <p>A burger without a patty or cheese is just plain ol' bread.</p>
    }
    return (
        <div className="Burger">
            <BurgerIngredient type="bread-top"></BurgerIngredient>
            {transformedIngredients}
            <BurgerIngredient type="bread-bottom"></BurgerIngredient>
        </div>
    );
}

export default Burger;
