import Aux from "../../../hoc/Aux";
import Button from "../../UI/Button/Button";

const OrderSummary = (props) => {
    const ingredientSummary = Object.keys(props.ingredients)
        .map(ingredientKey => {
            return (
                <li key={ingredientKey}>
                    <span style={{"textTransform": "capitalize"}}>{ingredientKey}</span>: {props.ingredients[ingredientKey]}
                </li>
            );
        });

    return (
        <Aux>
            <h3>Order Summary</h3>
            <p>Burger Delicious:</p>
            <ul>
                {ingredientSummary}
            </ul> 
            <p>Price: <strong>{props.price.toFixed(2)}</strong></p>
            <p>Continue to Checkout?</p>  
            <Button buttonType="Danger" clicked={props.purchaseCancelled}>Cancel</Button>     
            <Button buttonType="Success" clicked={props.purchaseContinued}>Continue</Button>       
        </Aux>
    );
}

export default OrderSummary;
